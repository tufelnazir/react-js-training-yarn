import userStore from '../stores/User';
import movieStore from '../stores/Movie';

export default () => {
  return {
    userStore,
    movieStore
  };
}